# Open Animation Layers, Adds a simpler layering system for animation.
# Copyright (C) 2020 bird_d <relay198@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

bl_info = {
    "name": "Open Animation Layers",
    "author": "bird_d",
    "version": (0, 1, 0),
    "blender": (2, 83, 0),
    "location": "Graph Editor/Dope Sheet > N-Panel > Open Animation Layers",
    "description": "Adds a ui for a layer-based workflow for working with animations that similar to other programs",
    "warning": "Incompatible with NLA-based workflows",
    "doc_url": "https://gitlab.com/bird_d/open_animation_layers/-/wikis/home",
    "category": "Animation",
}

import bpy
from bpy.props import IntProperty, BoolProperty
from bpy.types import AddonPreferences
from . import operators as operators
from . import ui as ui

class OpenAnimationLayersSettings(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__
    show_action_in_layers : BoolProperty(default=True, description="Shows each layer's action in the layer list")

    def draw(self, context):
        layout = self.layout.box()
        layout.prop(self, "show_action_in_layers", text="Show Actions")

#Callback to choose what happens when the layer index changes
def on_layer_index_change(self, context):
    obj = context.object
    anim_data = obj.animation_data
    if obj.active_nla_layer_index == obj.previous_nla_layer_index:
        return

    #Set the object's action as the first strip's action
    track = anim_data.nla_tracks[obj.active_nla_layer_index]
    strip = track.strips[0]
    anim_data.action = strip.action

    #Copy strip options
    #HACK: Doubt you'd really want to have combine instead of replace, 
    # it could break something? I can't find any other way to do this.
    anim_data.action_blend_type = strip.blend_type if strip.blend_type != 'REPLACE' else 'COMBINE'
    anim_data.action_extrapolation = strip.extrapolation 

    #See if the track is muted, and if so we need to set our current "layer"
    # AKA. the action we set as our object's action
    if track.mute:
        anim_data.action_influence = 0.0
    else:
        anim_data.action_influence = 1.0

    #We don't want the strip to have influence anymore, since we have the action
    # at the top of the nla's stack and don't want it applying twice
    strip.mute = True

    #See if previous layer still exists
    if obj.previous_nla_layer_index >= len(anim_data.nla_tracks):
        obj.previous_nla_layer_index = obj.active_nla_layer_index
        return
    #Undo change from previous layer selection
    anim_data.nla_tracks[obj.previous_nla_layer_index].strips[0].mute = False
    obj.previous_nla_layer_index = obj.active_nla_layer_index

#region    #Msgbus#

#Listen to mute changes so we can make the active action mute if the track gets muted, bit of a #HACK?
def msgbus_nla_mute_callback(*args):
    obj = bpy.context.object
    if obj.animation_data.nla_tracks[obj.active_nla_layer_index].mute:
        obj.animation_data.action_influence = 0.0
    else:
        obj.animation_data.action_influence = 1.0
#Same as mute, but for the strip's blend_type
def msgbus_nla_strip_blending(*args):
    obj = bpy.context.object
    anim_data = obj.animation_data
    strip = anim_data.nla_tracks[obj.active_nla_layer_index].strips[0]
    if anim_data.action_blend_type != strip.blend_type:
        anim_data.action_blend_type = strip.blend_type if strip.blend_type != 'REPLACE' else 'COMBINE'
    
nla_subscription_owner = object()

#Make it so we call a function when subscribe_to's data path updates
def subscribe_nla_properties(subscription_owner):
    subscribe_to = bpy.types.NlaTrack, "mute"
    bpy.msgbus.subscribe_rna(
        key=subscribe_to,
        owner=subscription_owner,
        args=(),
        notify=msgbus_nla_mute_callback,
        options={'PERSISTENT',}
    )

    subscribe_to = bpy.types.NlaStrip, "blend_type"
    bpy.msgbus.subscribe_rna(
        key=subscribe_to,
        owner=subscription_owner,
        args=(),
        notify=msgbus_nla_strip_blending,
        options={'PERSISTENT',}
    )

    if load_handler not in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(load_handler)

def unsubscribe_nla_properties(subscription_owner):
    if subscription_owner is not None:
        bpy.msgbus.clear_by_owner(subscription_owner)

    if load_handler in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(load_handler)

#Make it so everything is resubscribed if loading a new file
from bpy.app.handlers import persistent
@persistent
def load_handler(dummy):
    subscribe_nla_properties(nla_subscription_owner)

#endregion #Msgbus#

#region    #Registration#
classes = [
    OpenAnimationLayersSettings,
    #UI
    ui.GRAPH_PT_oal_layers,
    ui.DOPESHEET_PT_oal_layers,
    ui.NLATRACKS_UL_layer_list,
    #Operators
    operators.OAL_OT_new_layer,
    operators.OAL_OT_remove_layer
]

addon_keymaps = []

def register():
    #Register classes
    for cls in classes:
        bpy.utils.register_class(cls)
    
    #Adding common properties to every obj
    bpy.types.Object.active_nla_layer_index = IntProperty(default=0, update=on_layer_index_change)
    bpy.types.Object.previous_nla_layer_index = IntProperty(default=0)

    #Msgbus
    subscribe_nla_properties(nla_subscription_owner)

def unregister():
    #Remove common properties from every obj
    del bpy.types.Object.active_nla_layer_index
    del bpy.types.Object.previous_nla_layer_index

    #Unregister classes
    for cls in classes:
        bpy.utils.unregister_class(cls)

    #Msgbus
    unsubscribe_nla_properties(nla_subscription_owner)

#endregion #Registration#

if __name__ == "__main__":
    try:
        unregister()
    except RuntimeError as e:
        print(e)
    except AttributeError as e:
        print(e)
    except:
        import sys
        print("Error:", sys.exc_info()[0])
        pass
    register()